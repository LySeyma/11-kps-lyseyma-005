package com.company;

public class Main {
    public static void main(String[] args) {
        Thread t1 = new Thread(new Star());
        t1.start();
        try {
            t1.join(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
